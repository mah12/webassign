var tweets;

function create() {
	var m = document.createElement("H3");
	var n = document.createTextNode("Enter the username :  ");
	m.appendChild(n);
	document.body.appendChild(m);
	var inputTag = document.createElement("input");
	inputTag.setAttribute("type", "name");
	inputTag.setAttribute("id", "username");
	document.body.appendChild(inputTag);	
	var button = document.createElement("button");
	var n = document.createTextNode("Search");
	button.setAttribute("id", "button");
	button.appendChild(n);
	button.addEventListener("click", feedUserName);
	document.body.appendChild(button);
		 
}
function feedUserName(){
	var xhttp;
	var username = document.getElementById('username').value;
	xhttp = new XMLHttpRequest();
	xhttp.open("POST", "twitter.php", false);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xhttp.send("username=" + username);
	tweets = JSON.parse(xhttp.responseText);
	sendTweets(username);
}

function sendTweets(username){
	var i, p, t, j;
	var h = document.createElement("H3");
	var u = document.createTextNode("Recent 10 Tweets of " + username + " are: \n");
	h.appendChild(u);
	document.body.appendChild(h);
	var table = document.createElement("table");
	var tr, td, cell1, cell2;
	for(i = 0; i < tweets.length; i++) {	
		j = i + 1;
		tr = table.insertRow(-1);
		cell1 = tr.insertCell(0);
		cell1.innerHTML = "" + j;
		cell2 = tr.insertCell(1);
		cell2.innerHTML = tweets[i];
		table.appendChild(tr);
	}
	table.setAttribute("border","1");
	table.style.border = "4px solid red";
	document.body.appendChild(table);
	

}
function load() {
	create();
}
window.onload = load;
